import loader from './loader.svg';
import logo from './logo.svg';
import Vora from './VORA.png'
export {
  logo,
  loader,
  Vora
};
