import { configureStore, getDefaultMiddleware } from '@reduxjs/toolkit';

import playerReducer from './features/playerSlice';
import { shazemCoreApi } from './services/shazamCore';
export const store = configureStore({
  reducer: {
    [shazemCoreApi.reducerPath]:shazemCoreApi.reducer,
    player: playerReducer,
  },
  middleware:(getDefaultMiddleware)=>getDefaultMiddleware().concat(shazemCoreApi.middleware),
});
